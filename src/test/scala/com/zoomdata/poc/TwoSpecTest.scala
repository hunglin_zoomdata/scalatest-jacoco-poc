package com.zoomdata.poc

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest._

@RunWith(classOf[JUnitRunner])
class TwoSpecTest extends FlatSpec {

  "Two" should "run method1 and return 1" in {
    (new Two()).method1 === 1
  }

  "Two" should "run method2 and return \"2\"" in {
    (new Two()).method2 === 2
  }
}
